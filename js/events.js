let car = {
	manufacturer: 'Ford',
	model: 'Fusion',
	year: 2003,
	colour: 'grey',

	start: function() {
		this.trigger('starting', {
			time: '09:00',
			passengers: 2
		});
	}
};
_.extend(car, Backbone.Events);

car.on('starting', function(e) {
	console.log('The car started with parameters: ' + e);
})
