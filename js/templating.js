let Car = Backbone.Model.extend({});

let CarView = Backbone.View.extend({
	render: function() {
		let carTemplate = _.template($('#carTemplate').html());
		let html = carTemplate(this.model.toJSON());
		this.$el.html(html);
		return this;
	}
});

let car = new Car({manufacturer: 'Ford', model: 'Fusion', colour: 'Green'});
let carView = new CarView({
	el: '#cars',
	model: car
});
carView.render();