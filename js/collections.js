let Car = Backbone.Model.extend();
let Cars = Backbone.Collection.extend({
	model: Car
});

let cars = new Cars([
	new Car({manufacturer: 'Ford', model: 'Fusion', year: '2003'}),
	new Car({manufacturer: 'Toyota', model: 'Aygo', year: '2019'}),
	new Car({manufacturer: 'Kia', model: 'Ibiza', year: '2018'})
]);

console.log(cars.toJSON());

cars.add(new Car({manufacturer: 'Volkswagen', model: 'Golf', year: '2015'}));

console.log(cars.toJSON());

cars.remove(cars.at(0));

console.log(cars.toJSON());

console.log(cars.get('c3').toJSON());