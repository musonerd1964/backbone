let Car = Backbone.Model.extend({});

let car = new Car({manufacturer: 'Ford', model: 'Fusion', year: '2003', colour: 'Grey'});

let CarView = Backbone.View.extend({
	events: {
		'click': 'onclick',
		'click .bookmark': 'bookmark'
	},
	onclick: function() {
		console.log('Button clicked');
	},
	bookmark: function(e) {
		e.stopPropagation();
		console.log('Bookmarked');
	},
	render: function() {
		this.$el.html(this.model.get('manufacturer') + '<button>Click</button><button class="bookmark">Bookmark</button>');
	}
});

let carView = new CarView({
	el: '#cars',
	model: car
});
carView.render();