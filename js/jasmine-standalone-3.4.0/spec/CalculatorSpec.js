describe('Calculator', function() {
    let calculator;

    beforeEach(function() {
        calculator = new Calculator();
    });

    afterEach(function() {
        // code here
    });

    describe('add', function() {
        it('should be able to add two numbers', function() {
            let result = calculator.add(2, 3);

            expect(result).toEqual(5);
        });

        it('should have two arguments', function() {
            expect(function() {
                calculator.add(1);
            }).toThrow();
        });

        it('should be called with the right arguments', function() {
            spyOn(calculator, 'add');
            let result = calculator.add(2, 5);
            expect(result).toBeUndefined();
            expect(calculator.add).toHaveBeenCalled();
            expect(calculator.add).toHaveBeenCalledWith(2, 5);
        });

        it('mock server returned value', function() {
            spyOn(calculator, 'add').and.returnValue(7);
            let result = calculator.add(2, 5);
            expect(result).toEqual(7);
            expect(calculator.add).toHaveBeenCalled();
            expect(calculator.add).toHaveBeenCalledWith(2, 5);
        });

        it('mock throw error condition', function() {
            spyOn(calculator, 'add').and.throwError('Some Error');
            expect(function() {
                let result = calculator.add(2, 7);
            }).toThrowError('Some Error')
        });
    });
});
