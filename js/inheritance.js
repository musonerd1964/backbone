var Car = Backbone.Model.extend({
	info: function() {
		console.log('Base class');
	}
});

var car = new Car();
car.info();

var CarChild = Car.extend({
	info: function() {
		console.log('Child class');
	}
});

var carChild = new CarChild();
carChild.info();

var CarChild2 = Car.extend({
	info: function() {
		Car.prototype.info.apply(this.info);
	}
});

var carChild2 = new CarChild2();
carChild2.info();