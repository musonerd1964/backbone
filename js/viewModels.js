let Car = Backbone.Model.extend();

let Cars = Backbone.Collection.extend({
	model: Car
});

let CarView = Backbone.View.extend({
	tagName: 'li',
	render: function() {
		this.$el.html(this.model.get('manufacturer') + ', ' + this.model.get('model') + ', ' + this.model.get('year'));
		return this;
	}
});

let CarsView = Backbone.View.extend({
	tagName: 'ul',
	render: function() {
		let that = this;
		this.model.each(function(car) {
			let carView = new CarView({model: car});
			that.$el.append(carView.render().$el);
		});
	}
});

let cars = new Cars([
	new Car({manufacturer: 'Ford', model: 'Fusion', year: '2003'}),
	new Car({manufacturer: 'Toyota', model: 'Aygo', year: '2019'}),
	new Car({manufacturer: 'Kia', model: 'Ibiza', year: '2018'})
]);

let carsView = new CarsView({el: '#cars', model: cars});
carsView.render();