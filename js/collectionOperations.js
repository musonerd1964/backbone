let Car = Backbone.Model.extend();
let Cars = Backbone.Collection.extend({
	model: Car
});

let cars = new Cars([
	new Car({manufacturer: 'Ford', model: 'Fusion', year: '2003', colour: 'Grey'}),
	new Car({manufacturer: 'Toyota', model: 'Aygo', year: '2019', colour: 'Red'}),
	new Car({manufacturer: 'Kia', model: 'Ibiza', year: '2018', colour: 'Red'}),
	new Car({manufacturer: 'Ford', model: 'Fiesta', year: '2018', colour: 'White'})
]);

cars.add(new Car({manufacturer: 'Ferrari', model: 'Testerossa', year: '2012'}), {at: 1});
console.log(cars.toJSON());

cars.push(new Car({manufacturer: 'Rolls-Royce', model: 'Phantom', year: '1990', colour: 'Black'}));
console.log(cars.toJSON());

cars.pop();
console.log(cars.toJSON());

let redCars = cars.where({colour: 'Red'});
console.log('\nRed Cars: ');
redCars.forEach(x => {console.log(x.toJSON())});

let redCar = cars.findWhere({colour: 'Red'});
console.log('\nRed Car: ');
console.log(redCar.toJSON());

let newCars = cars.filter(x => {
	return x.get('year') > 2017;
});
console.log('\nNew Cars: ');
newCars.forEach(x => {console.log(x.toJSON())});