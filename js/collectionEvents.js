$('#cars').html('<ul id="carList"></ul>');

let Car = Backbone.Model.extend({});
let Cars = Backbone.Collection.extend({
    model: Car
});
let CarView = Backbone.View.extend({
    tagName: 'li',
    render: function() {
        this.$el.html(`${this.model.get('manufacturer')} ${this.model.get('model')} ${this.model.get('colour')}`);
        return this;
    }
});

let CarsView = Backbone.View.extend({
    tagName: 'ul',
    initialize: function() {
        this.model.on('add', this.onAdd, this);
    },
    onAdd: function() {
        this.$el.html('');
        this.render();
    },
    render: function() {
        let self = this;
        this.model.each((car) => {
            let carView = new CarView({model: car});
            self.$el.append(carView.render().$el);
        });
    }
});

let cars = new Cars([
    new Car({manufacturer: 'Ford', model: 'Fusion', colour: 'Grey'}),
    new Car({manufacturer: 'Toyota', model: 'Aygo', colour: 'Red'}),
    new Car({manufacturer: 'Seat', model: 'Ibiza', colour: 'White'})
]);

let carsView = new CarsView({
    el: '#carList',
    model: cars
});

carsView.render();
