var Car = Backbone.Model.extend({
	initialize: function() {
		console.log('Initialise');
	},
	defaults: {
		transmission: 'manual'
	},
	validate: function(attrs) {
		if (!attrs.model) {
			return 'Invalid - cars must have a model';
		}
	}
});

var car = new Car({car: 'Ford Fusion'});
car.set({date: '2003', colour: 'Grey'});
console.log(car.get('car'));
console.log(car.has('date'));
console.log(car.toJSON());
car.unset('colour');
console.log(car.toJSON());
car.clear();
console.log(car.toJSON());

var vehicle = new Car({manufacturer: 'Ford', model: 'Ford', year: '2003', colour: 'Grey'});
console.log(vehicle.toJSON());

var car1 = new Car({manufacturer: 'Toyota', year: '2010', colour: 'Red'});
console.log(car1.isValid());
console.log(car1.validationError);