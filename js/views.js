let View = Backbone.View.extend({
	render: function() {
		this.$el.html('Hello World');
		return this;
	}
});

let view = new View({el: '#collections'});
view.render();