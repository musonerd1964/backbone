define(
    [
        'underscore',
        'backbone',
        'models/car'
    ],
    function(_, Backbone, Car) {
        let Cars = Backbone.Collection.extend({model: Car});
        return Cars;
    }
);


