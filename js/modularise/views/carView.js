define(
    [
        'jquery',
        'underscore',
        'backbone',
        'models/car'
    ],
    function($, _, Backbone, Car) {
        let CarView = Backbone.View.extend({
            render: function() {
                this.$el.html(`${this.model.get('manufacturer')}, ${this.model.get('model')}, ${this.model.get('year')}`);
                return this;
            }
        });
        return CarView;
    }
);


