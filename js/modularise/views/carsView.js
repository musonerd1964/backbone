define(
    [
        'jquery',
        'underscore',
        'backbone',
        'models/car',
        'collections/cars'
    ],
    function($, _, Backbone, Cars) {
        let CarsView = Backbone.View.extend({
            render: function() {
                let self = this;
                this.collection.each(function(car) {
                    let carView = new CarView({model: car});
                    self.$el.append(carView.render().$el);
                });
            }
        });
        return CarsView;
    }
);


