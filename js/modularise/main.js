require.config({
    paths: {
        jquery: 'lib/jquery-3.4.1.min.js',
        underscore: 'lib/underscore-1.9.1.min.js',
        backbone: 'lib/backbone-1.4.0.min.js'
    }
});

define(
    ['app'],
    function(App) {
        App.initialize();
    }
);
