define(
    [
        'underscore',
        'backbone',
        'model/car',
        'collection/cars',
        'views/carsView'
    ], function(_, Backbone, Car, Cars, CarsView) {
        let initialize = function() {
            let cars = new Cars([
                new Car({manufacturer: 'Ford', model: 'Fusion', year: '2003'}),
                new Car({manufacturer: 'Toyota', model: 'Aygo', year: '2019'}),
                new Car({manufacturer: 'Kia', model: 'Ibiza', year: '2018'})
            ]);

            let carsView = new CarsView({el: '#cars', collection: cars});
            carsView.render();
        }

        return {
            initialize: initialize
        }
    }
);

