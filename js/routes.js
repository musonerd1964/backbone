let AlbumView = Backbone.View.extend({
	render: function() {
		this.$el.html('Album View');
		return this;
	}
});

let ArtistView = Backbone.View.extend({
	render: function() {
		this.$el.html('Artist View');
		return this;
	}
});

let GenreView = Backbone.View.extend({
	render: function() {
		this.$el.html('Genre View');
		return this;
	}
});

let OtherView = Backbone.View.extend({
	render: function() {
		this.$el.html('Page not found');
		return this;
	}
});

let Router = Backbone.Router.extend({
	routes: {
		'albums': 'ViewAlbum',
		'artists': 'ViewArtist',
		'genres': 'ViewGenre',
		'*other': 'ViewDefault'
	},
	ViewAlbum: function() {
		let view = new AlbumView({el: '#cars'});
		view.render();
	},
	ViewArtist: function() {
		let view = new ArtistView({el: '#cars'});
		view.render();
	},
	ViewGenre: function() {
		let view = new GenreView({el: '#cars'});
		view.render();
	},
	ViewDefault: function() {
		let view = new OtherView({el: '#cars'});
		view.render();
	}
});

let router = new Router();
Backbone.history.start();

let NavView = Backbone.View.extend({
	events: {
		'click': 'onClick'
	},
	onClick: function(e) {
		let $li = $(e.target);
		router.navigate($li.attr('data-url'), {trigger: true});
	}
});

let navView = new NavView({el: '#navBar'});
