let Car = Backbone.Model.extend({

});

let CarView = Backbone.View.extend({
    initialize: function() {
        this.model.on('change', this.onModelChange, this);
    },
    onModelChange: function() {
        this.render();
    },
    render: function() {
        this.$el.html(`Manufacturer: ${this.model.get('manufacturer')}, Model: ${this.model.get('model')}, Colour: ${this.model.get('colour')}`);
        return this;
    }
});

let car = new Car({manufacturer: 'Ford', model: 'Fusion', colour: 'Gunmetal Grey'});
let carView = new CarView({
    el: '#cars',
    model: car
});
carView.render();
