let Item = Backbone.Model.extend({
    validate: function(attrs) {
        if (!attrs.manufacturer) {
            return 'Manufacturer property of item is mandatory'
        }
    }
});
