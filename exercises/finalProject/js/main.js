$(document).ready(function() {
    let items = new Items([
        new Item({manufacturer: 'Ford', registration: 'KF53 HZA'}),
        new Item({manufacturer: 'Toyota', registration: 'ABC1 23D'}),
        new Item({manufacturer: 'Seat', registration: 'ZYX9 87W'})
    ]);

    let itemsView = new ItemsView({model: items});
    $('body').append(itemsView.render().$el);
});


