let ItemView = Backbone.View.extend({
    tagName: 'li',
    initialize: function(options) {
        if (!(options && options.model)) {
            throw new Error('Item view must be instantiated with a model');
        }
    },
    render: function() {
        this.$el.html(this.model.get('manufacturer'));
        return this;
    }
});

