let ItemsView = Backbone.View.extend({
    tagName: 'ul',
    id: 'itemsList',
    initialize: function(options) {
        if (!(options && options.model)) {
            throw new Error('Items view must be instantiated with a model');
        }
    },
    render: function() {
        let self = this;
        this.model.each(function(item) {
            let view = new ItemView({model: item});
            self.$el.append(view.render().$el);
        });
        return this;
    }
});
