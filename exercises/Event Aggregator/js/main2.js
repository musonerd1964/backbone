let Venue = Backbone.Model.extend({});
let Venues = Backbone.Collection.extend({
	model: Venue
});

let VenueView = Backbone.View.extend({
	tabName: 'li',

	events: {
		'click': 'onClick'
	},

	initialize: function(options) {
		this.bus = options.bus;
	},

	onClick: function() {
		this.bus.trigger('venueSelected', this.model);
	},

	render: function() {
		this.$el.html(this.model.get('name'));
		return this;
	}
});

let VenuesView = Backbone.View.extend({
	tabName: 'ul',

	initialize: function(options) {
		this.bus = options.bus;
	},
	
	render: function() {
		let self = this;
		this.model.each(function(venue) {
			let view = new VenueView({model: venue, bus: self.bus});
			self.$el.append(view.render().$el);
		});
		return this;
	}
});

let MapView = Backbone.View.extend({
	el: '#map-container',

	initialize: function(options) {
		this.bus = options.bus;
		this.bus.on('venueSelected', this.onVenueSelected, this);
	},

	onVenueSelected: function(venue) {
		this.model = venue;
		this.render();
	},

	render: function() {
		if (this.model) {
			this.$("#venue-name").html(this.model.get('name'));
		}
		return this;
	}
});

let bus = _.extend({}, Backbone.Events);

let venues = new Venues([
	new Venue({ name: "30 Mill Espresso" }),
	new Venue({ name: "Platform Espresso" }),
	new Venue({ name: "Mr Foxx" })
]);

var venuesView = new VenuesView({ model: venues, bus: bus});
$("#venues-container").html(venuesView.render().$el);

let mapView = new MapView({bus: bus});
mapView.render();