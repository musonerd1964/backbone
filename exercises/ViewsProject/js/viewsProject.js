$('#cars').html('<ul id="carList"></ul>');
let Car = Backbone.Model.extend({});
let Cars = Backbone.Collection.extend({
	model: Car
});

let CarView = Backbone.View.extend({
	tagName: 'li',
	render: function() {
		let carTemplate = _.template($('#carTemplate').html());
		let html = carTemplate(this.model.toJSON());
		this.$el.html(html);

		this.$el.attr({'class': 'carRows', 'data-color': 'red'});
		return this;
	}
});

let CarsView = Backbone.View.extend({
	tagname: 'ul',
	events: {
		'click': 'onClick'
	},
	onClick: function(e) {
		$(e.target).closest('li').remove();
	},
	render: function() {
		let self = this;
		this.model.each(function(car) {
			let carView = new CarView({model: car});
			self.$el.append(carView.render().$el.prop('class', 'vehicle'));
		});
	}
});

let cars = new Cars([
    new Car({manufacturer: 'Ford', model: 'Fusion', colour: 'Grey', registration: 'KF53HZA'}),
    new Car({manufacturer: 'Toyota', model: 'Aygo', colour: 'Red', registration: 'COB123B'}),
    new Car({manufacturer: 'Seat', model: 'Ibiza', colour: 'White', registration: 'ABC123C'})
]);

let carsView = new CarsView({
	el: '#carList',
	model: cars
});
carsView.render();