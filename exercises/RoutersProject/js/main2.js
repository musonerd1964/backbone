let Vehicle = Backbone.Model.extend({});
let Vehicles = Backbone.Collection.extend({
    model: Vehicle
});

let cars = new Vehicles([
    new Vehicle({registration: 'KF53HZA', colour: 'grey'}),
    new Vehicle({registration: 'ABC123D', colour: 'red'}),
    new Vehicle({registration: 'ZYX987W', colour: 'orange'})
]);

let boats = new Vehicles([
    new Vehicle({registration: 'ABC123D', colour: 'yellow'}),
    new Vehicle({registration: 'DEF456G', colour: 'green'}),
    new Vehicle({registration: 'HIJ789K', colour: 'pink'})
]);

let VehicleView = Backbone.View.extend({
    render: function() {
        this.$el.html(this.model.get('registration') + ', ' + this.model.get('colour'));
        return this;
    }
});

let VehiclesView = Backbone.View.extend({
    render: function() {
        let self = this;
        this.collection.each(function(vehicle) {
            let view = new VehicleView({model: vehicle});
            self.$el.append(view.render().$el);
        });
    }
});

let HomeView = Backbone.View.extend({
    render: function() {
        this.$el.html('Home View');
        return this;
    }
});

let NavView = Backbone.View.extend({
    events: {
        'click': 'onClick'
    },
    onClick: function(e) {
        let $li = $(e.target);
        router.navigate($li.attr('data-url'), {trigger: true});
    }
});

let Router = Backbone.Router.extend({
    routes: {
        'cars': 'ViewCars',
        'boats': 'ViewBoats',
        '*other': 'ViewHome'
    },
    ViewCars: function() {
        this.loadView(new VehiclesView({collection: cars, el: '#container'}));
    },
    ViewBoats: function() {
        this.loadView(new VehiclesView({collection: boats, el: '#container'}));
    },
    ViewHome: function() {
        this.loadView(new HomeView({el: '#container'}));
    },
    loadView: function(view) {
        $('#container').html('');
        view.render();
    }
});

let router = new Router();
Backbone.history.start();

var navView = new NavView({ el: "#nav" });
