var Vehicle = Backbone.Model.extend({});
var vehicle = new Vehicle();

var Vehicles = Backbone.Collection.extend({
model: vehicle
});

var vehicles = new Vehicles([
new Vehicle({registrationNumber: 'XLI887', colour: 'Blue'}),
new Vehicle({registrationNumber: 'ZNP123', colour: 'Blue'}),
new Vehicle({registrationNumber: 'XUV456', colour: 'Grey'})
]);

console.log('All vehicles: ');
vehicles.forEach(x => {
console.log(x.toJSON());
});

var blueCars = vehicles.filter(x => {
return x.get('colour') === 'Blue';
});

console.log('Blue cars: ');
blueCars.forEach(x => {
console.log(x.toJSON());
});

var regCar = vehicles.findWhere({registrationNumber: 'XLI887'});
console.log('Car with registration number XLI887: ');
console.log(regCar.toJSON());

console.log('Filtered vehicles: ');
vehicles.remove(regCar);
vehicles.forEach(x => {
console.log(x.toJSON());
});