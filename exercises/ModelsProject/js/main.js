let Vehicle = Backbone.Model.extend({
	start: function() {
		console.log('Vehicle started');
	},
	validate: function(attrs) {
		if (!attrs.registrationNumber) {
			return 'Registration number is mandatory';
		}
	}

});

let vehicle = new Vehicle({manufacturer: 'Ford', colour: 'Grey'});

let Vehicle2 = Vehicle.extend({
	start: function() {
		console.log(`Car with registration number ${this.get('registrationNumber')} started.`);
	}

});

let vehicle2 = new Vehicle2({manufacturer: 'Ford', model: 'Fusion', registrationNumber: 'KF53 HZA'});

vehicle2.start();
vehicle2.unset('registrationNumber');
console.log(vehicle2.toJSON());
console.log('Is vehicle valid: ' + vehicle2.isValid());
console.log('Invalid vehicle validation error: ' + vehicle2.validationError);

vehicle2.set({registrationNumber: 'KF53 HZA'});
console.log(vehicle2.toJSON());
console.log('Is vehicle valid: ' + vehicle2.isValid());

vehicle2.start();