define(
	[
		'underscore',
		'backbone'
	],
	function(_, Backbone, Car) {
		var Car = Backbone.Model.extend({});
		return Car;
	}
);
