define(
	[
		'underscore',
		'backbone',
		'models/carModel',
		'collections/carCollection',
		'views/carView'
	], 
	function(_, Backbone, Car, Cars, CarView) {
		var initialize = function() {
			var cars = new Cars([
				new Car({manufacturer: 'Ford', model: 'Fusion', year: 2003, colour: 'grey'}),
				new Car({manufacturer: 'Toyota', model: 'Aygo', year: 2019, colour: 'red'}),
				new Car({manufacturer: 'Ford', model: 'Fiesta', year: 2018, colour: 'red'})
			]);

			var carView = new CarView({el: '#container', model: cars.at(0)});
			carView.render();
		}

		return {
			initialize: initialize
		}
	}
);