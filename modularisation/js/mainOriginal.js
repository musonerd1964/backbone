let Car = Backbone.Model.extend({});

let Cars = Backbone.Collection.extend({
	model: Car
});

let CarView = Backbone.View.extend({
	render: function() {
		this.$el.html(this.model.get('model'));
		return this;
	}
});

let cars = new Cars([
	new Car({manufacturer: 'Ford', model: 'Fusion', year: 2003, colour: 'grey'}),
	new Car({manufacturer: 'Toyota', model: 'Aygo', year: 2019, colour: 'red'}),
	new Car({manufacturer: 'Ford', model: 'Fiesta', year: 2018, colour: 'red'}),
]);

let carView = new CarView({el: '#container', model: cars.at(0)});
carView.render();