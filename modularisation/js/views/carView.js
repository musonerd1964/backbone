define(
	[
		'underscore',
		'backbone',
		'jquery',
		'models/carModel'
	],
	function(_, Backbone, $, Car) {
		var CarView = Backbone.View.extend({
			render: function() {
				this.$el.html(this.model.get('model'));
				return this;
			}
		});

		return CarView;
	}
);

