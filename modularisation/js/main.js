require.config({
	paths: {
		underscore: 'lib/underscore-1.9.1.min',
		backbone: 'lib/backbone-1.4.0.min',
		jquery: 'lib/jquery-3.4.1.min'
	}
});

define(
	['app'], 
	function(App) {
		App.initialize();
	}
);
