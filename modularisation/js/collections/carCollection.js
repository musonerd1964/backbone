define(
	[
		'underscore',
		'backbone',
		'models/carModel'
	],
	function(_, Backbone, Car) {
		let Cars = Backbone.Collection.extend({
			model: Car
		});

		return Cars;
	}
);
